import { createStore } from "vuex";

// Create a new store instance.
const store = createStore({
  state() {
    return {
      keycloak: null,
    };
  },
  mutations: {
    setKeycloak(state, keycloak) {
      state.keycloak = keycloak;
    },
  },
});
export default store;
