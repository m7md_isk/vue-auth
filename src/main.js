import { createApp } from "vue";
import App from "./App.vue";
import Keycloak from "keycloak-js";
import router from "./router";
import store from "./store";

let initOptions = {
  url: "https://sso-zainiac.zain.com/auth",
  realm: "ZainiacDev",
  clientId: "spa",
  onLoad: "login-required",
  pkceMethod: "S256",
};

let keycloak = Keycloak(initOptions);
keycloak
  .init({ onLoad: initOptions.onLoad })
  .then((auth) => {
    if (!auth) {
      window.location.reload();
    } else {
      console.log("authentecated");

      const app = createApp(App, { keycloak })
        .use(router)
        .use(store)
        .mount("#app");
    }

    setInterval(() => {
      keycloak
        .updateToken(70)
        .then((refreshed) => {
          if (refreshed) {
            console.log("Token refreshed: " + refreshed);
          }
        })
        .catch(() => {
          console.error("Failed to refresh token");
        });
    }, 6000);
  })
  .catch(() => {
    console.error("Authenticated Failed");
  });
